#   INGEGNERIA DEL SOFTWARE 2 -> Cloud and Edge Computing

#  Titolo: Seat&Meals API 


## Descrizione 

----

![NPM Version](https://img.shields.io/badge/Made%20with-node%20v16.13.1-yellow)

>Il software permette all' utente di visualizzare e prenotare i posti disponibili all'interno della mensa universitaria. Inoltre consente all' utilizzatore di consultare il menù in una determinata data. Garantisce la possibilità di lasciare una recensione al pasto che l'utilizzatore ha consumato. Infine consente all'amministratore di aggiungere e rimuovere utenti al sistema.


## Api

----

>1. Identificazione utente
>
> Api che permette di inserire utenti e visualizzare la lista degli utenti registrati al sistema.

>2. Visualizzazione posti in mensa con differenziazione libero/occupato 
>
> Api che permette all' utente di visualizzare i posti a sedere presenti all'interno della mensa effettuando una distinzione tra quelli liberi e quelli occupati. 

>3. Prenotazione dei posti
>
> Api che permette all' utente di prenotare un determinato posto a sedere restituendo una conferma di avvenuta azione. Quest'ultima non andrebbe a buon fine nel caso in cui il posto risultasse essere già occupato.

>4. Visualizzazione menù
>
> Api che permette all'utente di visualizzare il menù disponibile nel giorno di accesso al servizio.

>5. Review dei pasti
>
> Api che permette all'utente di creare una recensione del pasto che ha consumato 

## Documentazione su Apiary

-----

[Seats&Meals](https://sendnodes.docs.apiary.io)

## Applicazione su Heroku

----

https://cloudedgecomputing.herokuapp.com

##  Git repository e ProductBacklog link

----

La Git Repository originale usata per lo sviluppo del progetto è la seguente:

[ingso2](https://github.com/Spanix98/ingso2/tree/master)

La Git Repository relativa all'implementazione del meccanismo di CI/CD di GitLab è la seguente:

[cloudedgecomputing](https://gitlab.com/286006/ingso2)


##  Componenti del gruppo

----

Per quanto riguarda il progetto originale viene indicato ciascun componente del gruppo con il prorio username su Github.

- **Francesco Di Flumeri** - francesco9820
- **Fabio Spanò** - Spanix98
- **Antonio Piccolo** - antoniopiccolounitn
- **Alessandro Menghini** - xLizhh
- **Damiano Duranti** - damianoduranti

La repository su GitLab, invece, è stata creata e gestita unicamente da **Fabio Spanò** - 286006


<!-- Markdown link & img dfn's -->
[npm-image]: https://img.shields.io/badge/Made%20with%3A-node%20v12.13.0-yellowgreen

