describe('POST /meals', function() {
    it('saves a new meal in order to review it', function(done) {
        let meal = {
            first: 'Pasta al pesto',
            second: 'Scaloppine al vino bianco',
            dessert: 'Tiramisù',
            date: '15-03-2021'
        }
      request.post('/api/v1/meals')
        .query(meal)
        .expect(201)
        .end(function(err, res) {
          done(err);
        });

    });
  });

  describe('POST /reviews', function() {
    it('review a meal', function(done) {
        let review = {
            review: 'Il pasto era meh.',
            mealDate: '15-03-2021'
        }
      request.post('/api/v1/reviews')
        .query(review)
        .expect(201)
        .end(function(err, res) {
          done(err);
        });

    });
  });

  describe('POST /reviews', function() {
    it('review a future meal', function(done) {
        let review = {
            review: 'Il pasto era meh.',
            mealDate: '15-03-2022'
        }
      request.post('/api/v1/reviews')
        .query(review)
        .expect(400)
        .end(function(err, res) {
          done(err);
        });

    });
  });

  describe('GET /reviews', function() {
    it('return the list of reviews', function(done) {
      request.get('/api/v1/reviews')
        .expect(200)
        .end(function(err, res) {
          done(err);
        });
    });
  });

  describe('GET /reviews/:reviewid', function() {
    var reviewid=0;
    it('return the list of reviews', function(done) {
      request.get('/api/v1/reviews')
        .expect(200)
        .end(function(err, res) {
            reviewid=res.body[0][0].reviewid;
          done(err);
        });
    });
    it('return a specific review', function(done) {
        request.get('/api/v1/reviews/' + reviewid)
          .expect(200)
          .end(function(err, res) {
            done(err);
          });
      });
  });

  describe('DELETE /reviews', function() {
    it('delete the list of reviews', function(done) {
      request.delete('/api/v1/reviews')
        .expect(200)
        .end(function(err, res) {
          done(err);
        });
    });
  });

  describe('GET /reviews', function() {
    it('return the empty list of reviews', function(done) {
      request.get('/api/v1/reviews')
        .expect(404)
        .end(function(err, res) {
          done(err);
        });
    });
  });

  describe('GET /reviews/:reviewid', function() {
    var reviewid=0;
    it('return a non existing review', function(done) {
        request.get('/api/v1/reviews/' + reviewid)
          .expect(404)
          .end(function(err, res) {
            done(err);
          });
      });
  });