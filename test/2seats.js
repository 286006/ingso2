describe('POST /seats', function() {
    it('creates n seats in order to book them', function(done) {
        let nseats = 10
      request.post('/api/v1/seats/' + nseats)
        .expect(201)
        .end(function(err, res) {
          done(err);
        });

    });
  });

  describe('GET /seats', function() {
    it('return the list of seats with their informations', function(done) {
      request.get('/api/v1/seats')
        .expect(200)
        .end(function(err, res) {
          done(err);
        });
    });
  });

  describe('DELETE /seats', function() {
    it('delete every seat', function(done) {
      request.delete('/api/v1/seats')
        .expect(200)
        .end(function(err, res) {
          done(err);
        });
    });
  });

  describe('POST /seats', function() {
    it('creates an invalid value of seats', function(done) {
        let nseats = 'abc'
      request.post('/api/v1/seats/' + nseats)
        .expect(400)
        .end(function(err, res) {
          done(err);
        });

    });
  });

  describe('GET /seats', function() {
    it('return an empty list of seats', function(done) {
      request.get('/api/v1/seats')
        .expect(404)
        .end(function(err, res) {
          done(err);
        });
    });
  });
