describe('POST /seats', function() {
    it('creates n seats in order to book them', function(done) {
        let nseats = 10
      request.post('/api/v1/seats/' + nseats)
        .expect(201)
        .end(function(err, res) {
          done(err);
        });

    });
  });

  describe('POST /users', function() {
    it('saves user in order to book a seat', function(done) {
        let user = {
            mat: '123456',
            email: "prova@mail.it"
        }
      request.post('/api/v1/users')
        .query(user)
        .expect(201)
        .end(function(err, res) {
          done(err);
        });

    });
  });

  describe('POST /books/:id', function() {
    it('user id books the seatid seat', function(done) {
        let id = 123456
        let seat = {
          seatid: 1
      }
      request.post('/api/v1/books/'+ id)
        .query(seat)
        .expect(201)
        .end(function(err, res) {
          done(err);
        });
    });
  });

  describe('GET /books', function() {
    it('return the list of bookings', function(done) {
      request.get('/api/v1/books')
        .expect(200)
        .end(function(err, res) {
          done(err);
        });
    });
  });

  describe('GET /seats', function() {
    it('return the list of seats with their informations', function(done) {
      request.get('/api/v1/seats')
        .expect(200)
        .end(function(err, res) {
          done(err);
        });
    });
  });

  describe('POST /books/:id', function() {
    it('user id books a non existent seat', function(done) {
        let id = 123456
        let seatid = 11111111
      request.post('/api/v1/books/'+ id)
        .query(seatid)
        .expect(400)
        .end(function(err, res) {
          done(err);
        });
    });
  });

  describe('POST /books/:id', function() {
    it('non existing id books a seat', function(done) {
        let id = 987654
        let seatid = 2
      request.post('/api/v1/books/'+ id)
        .query(seatid)
        .expect(404)
        .end(function(err, res) {
          done(err);
        });
    });
  });
  describe('POST /books/:id', function() {
    it('user id books an already booked seat', function(done) {
        let id = 123456
        let seatid = 1
      request.post('/api/v1/books/'+ id)
        .query(seatid)
        .expect(400)
        .end(function(err, res) {
          done(err);
        });
    });
  });

  describe('GET /books/:id', function() {
    it('return the list of bookings of user id', function(done) {
        let id = 123456
      request.get('/api/v1/books/' + id)
        .expect(200)
        .end(function(err, res) {
          done(err);
        });
    });
  });

  describe('PUT /books/:user_mat', function() {

    var bookingid = 0;
    var id = 123456;
    it('get book reservation', function(done) {
      request.get('/api/v1/books/' + id)
        .expect(200)
        .end(function(err, res) {
           bookingid=res.body[0][0].id;
           done(err);
        });
    });

    it('change seat reservation with another seat', function(done) {
      let book = {
        bookid: bookingid,
        seatid: 3
      }
      request.put('/api/v1/books/' + id)
      .query(book)
      .expect(201)
      .end(function(err, res) {
        done(err);
      });
  });
});





  describe('PUT /books/:user_mat', function() {

    var bookingid = 0;
    var id = 123456;

    it('change inexistent seat reservation', function(done) {
      let book = {
        bookid: bookingid,
        seatid: 3
      }
      request.put('/api/v1/books/' + id)
      .query(book)
      .expect(400)
      .end(function(err, res) {
        done(err);
      });
    });
  });

  describe('DELETE /books', function() {
    it('delete the list of bookings', function(done) {
      request.delete('/api/v1/books')
        .expect(200)
        .end(function(err, res) {
          done(err);
        });
    });
  });

  describe('GET /books', function() {
    it('return the list of bookings', function(done) {
      request.get('/api/v1/books')
        .expect(404)
        .end(function(err, res) {
          done(err);
        });
    });
  });

  describe('GET /books/:id', function() {
    it('return the list of bookings of a non existing user id', function(done) {
        let id = 987654
      request.get('/api/v1/books/' + id)
        .expect(404)
        .end(function(err, res) {
          done(err);
        });
    });
  });