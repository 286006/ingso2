describe('POST /users', function() {
    it('saves a new user', function(done) {
        let user = {
            mat: '123456',
            email: "prova@mail.it"
        }
      request.post('/api/v1/users')
        .query(user)
        .expect(201)
        .end(function(err, res) {
          done(err);
        });

    });
  });

describe('GET /users', function() {
    it('return a list of users', function(done) {
      request.get('/api/v1/users')
        .expect(200)
        .end(function(err, res) {
          done(err);
        });
    });
  });


describe('GET /users/:id', function() {
    it('returns a user by mat', function(done) {
        let id = 123456
      request.get('/api/v1/users/' + id)
        .expect(200)
        .end(function(err, res) {
          done(err);
        });
    });
});

describe('PUT /users', function() {
    it('change email of a specific user', function(done) {
        let user = {
            mat: '123456',
            email: "prova2@mail.it"
        }
      request.put('/api/v1/users')
      .query(user)
        .expect(201)
        .end(function(err, res) {
          done(err);
        });
    });
  });

  describe('PUT /users', function() {
    it('try to change email of a non existent user', function(done) {
        let user = {
            mat: '987654',
            email: "prova2@mail.it"
        }
      request.put('/api/v1/users')
      .query(user)
        .expect(400)
        .end(function(err, res) {
          done(err);
        });
    });
  });

  describe('DELETE /users/:id', function() {
    it('removes a specific user', function(done) {
        let id = 123456
      request.delete('/api/v1/users/' + id)
        .expect(200)
        .end(function(err, res) {
          done(err);
        });
    });
  });

  describe('DELETE /users/:id', function() {
    it('removes a non existent user', function(done) {
        let id = 123456
      request.delete('/api/v1/users/' + id)
        .expect(404)
        .end(function(err, res) {
          done(err);
        });
    });
  });


describe('DELETE /users', function() {
    it('removes all users', function(done) {
      request.delete('/api/v1/users')
        .expect(200)
        .end(function(err, res) {
          done(err);
        });
    });
  });

describe('GET /users', function() {
    it('return a list of users when the list is empty', function(done) {
      request.get('/api/v1/users')
        .expect(404)
        .end(function(err, res) {
          done(err);
        });
    });
  });


describe('GET /users/:id', function() {
    it('returns a non existing user by mat', function(done) {
        let id = 123456
      request.get('/api/v1/users/' + id)
        .expect(404)
        .end(function(err, res) {
          done(err);
        });
    });
});

describe('POST /users', function() {
    it('saves a new user with wrong email (no .)', function(done) {
        let user = {
            mat: '123456',
            email: "prova@mail"
        }
      request.post('/api/v1/users')
        .query(user)
        .expect(400)
        .end(function(err, res) {
          done(err);
        });

    });
  });

  describe('POST /users', function() {
    it('saves a new user with wrong email (no @)', function(done) {
        let user = {
            mat: '123456',
            email: "provamail.it"
        }
      request.post('/api/v1/users')
        .query(user)
        .expect(400)
        .end(function(err, res) {
          done(err);
        });

    });
  });



