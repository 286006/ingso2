describe('POST /meals', function() {
    it('saves a new meal', function(done) {
        let meal = {
            first: 'Pasta al pesto',
            second: 'Scaloppine al vino bianco',
            dessert: 'Tiramisù',
            date: '15-03-2022'
        }
      request.post('/api/v1/meals')
        .query(meal)
        .expect(201)
        .end(function(err, res) {
          done(err);
        });

    });
  });

  describe('GET /meals', function() {
    it('return the list of meals', function(done) {
      request.get('/api/v1/meals')
        .expect(200)
        .end(function(err, res) {
          done(err);
        });
    });
  });

  describe('GET /meals/:date', function() {
    it('return the list of meals of a specific date', function(done) {
        let date = '15-03-2022'
      request.get('/api/v1/meals/' + date)
        .expect(200)
        .end(function(err, res) {
          done(err);
        });
    });
  });

  describe('GET /meals/:date', function() {
    it('return the list of meals of a date without meals', function(done) {
        let date = '15-03-2023'
      request.get('/api/v1/meals/' + date)
        .expect(404)
        .end(function(err, res) {
          done(err);
        });
    });
  });

  describe('DELETE /meals', function() {
    it('delete the list of meals', function(done) {
      request.delete('/api/v1/meals')
        .expect(200)
        .end(function(err, res) {
          done(err);
        });
    });
  });

  describe('GET /meals', function() {
    it('return empty list of meals', function(done) {
      request.get('/api/v1/meals')
        .expect(404)
        .end(function(err, res) {
          done(err);
        });
    });
  });

  describe('POST /meals', function() {
    it('meal with wrong date', function(done) {
        let meal = {
            first: 'Pasta al pesto',
            second: 'Scaloppine al vino bianco',
            dessert: 'Tiramisù',
            date: '1503-2022'
        }
      request.post('/api/v1/meals')
        .query(meal)
        .expect(400)
        .end(function(err, res) {
          done(err);
        });

    });
  });

  describe('POST /meals', function() {
    it('meal without a field', function(done) {
        let meal = {
            first: 'Pasta al pesto',
            second: 'Scaloppine al vino bianco',
            date: '15-03-2022'
        }
      request.post('/api/v1/meals')
        .query(meal)
        .expect(400)
        .end(function(err, res) {
          done(err);
        });

    });
  });