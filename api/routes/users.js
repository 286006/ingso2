const express = require('express');
const usersRoutes = express.Router();

const User = require('../models/users');
const UtilEmail = require ('../util/utilEmail')

usersRoutes.route('/')
	.get(async function(req, res) {

		// #swagger.description = 'Visualizza tutti gli utenti.'
        // #swagger.tags = ['Users']

		try{
			let users = await User.find({})
			if(users != null){
				res.status(200);
				res.json([users, {message: 'List of users found!'}]);
			}else{
				res.status(404);
				res.json({message: 'No users found!'});
			}
			
		}catch(error)/* istanbul ignore next */{
			
			res.status(500);
			res.json({message: 'ERROR 500: Local server error!'});
		}
	});
usersRoutes.route('/')
	.post(async function(req,res){

		// #swagger.description = 'Inserisci un utente.'
        // #swagger.tags = ['Users']
		/* #swagger.parameters['mat'] = { 
            description: 'La matricola dell\'utente.',
            type: 'int'
        } */
		/* #swagger.parameters['email'] = { 
            description: 'L\'email dell\'utente.',
            type: 'string'
        } */

		try{
			var user = new User();
			var mat = req.query.mat;
			user.email = req.query.email;
			var saved = null;

			if(mat != null && user.email != null && UtilEmail.validateEmail(user.email))		
				saved = await user.save(mat);
			if(saved != null){
				res.status(201);
				res.json([saved , {message: 'User correctly created'}]);
			}else{
				res.status(400);
				res.json({message: 'ERROR 400: User not created maybe for a mistake in the query params!'});
			}
		}catch(error)/* istanbul ignore next */{
			res.status(500);
			res.json({message: 'ERROR 500: Local server error!'});
		}
	});
usersRoutes.route('/')
	.delete(function (req, res) {

		// #swagger.description = 'Elimina tutti gli utenti.'
        // #swagger.tags = ['Users']

		try{
			/* istanbul ignore next */
			if(User.delete()){
				res.status(200);
				res.json({message: 'All users are cancelled'});
			}
		}catch(error)/* istanbul ignore next */{
			res.status(500);
			res.json({message: 'ERROR 500: Local server error!'});
		}
	});

usersRoutes.route('/:user_mat')
	.get(function(req,res){

		// #swagger.description = 'Visualizza un utente specifico.'
        // #swagger.tags = ['Users']
		/* #swagger.parameters['user_mat'] = { 
            description: 'La matricola dell\'utente.',
            type: 'int'
        } */

		try{
			let users_m = User.findByMatricola(req.params.user_mat);
			if(users_m != null){
				res.status(200);
				res.json([users_m, {message: 'User correctly found!'}])
			}else{
				res.status(404);
				res.json({message: 'ERROR 404: User not found'});
			}
		}catch(error)/* istanbul ignore next */{
			res.status(500);
			res.json({message: 'ERROR 500: Local server error!'});
		}
	})
	.delete(function(req,res){

		// #swagger.description = 'Elimina un utente specifico.'
        // #swagger.tags = ['Users']
		/* #swagger.parameters['user_mat'] = { 
            description: 'La matricola dell\'utente.',
            type: 'int'
        } */

		try{
			if(User.remove(req.params.user_mat)){
				res.status(200);
				res.json({message: 'Successfully deleted'});
			}else{
				res.status(404);
				res.json({message: 'ERROR 404: Invalid matricola'});
			}
		}catch(error)/* istanbul ignore next */{
			res.status(500);
			res.json({message: 'ERROR 500: Local server error!'});
		}
	});
usersRoutes.route('/')
	.put(async function(req,res){

		// #swagger.description = 'Modifica l\'indirizzo email di un utente specifico.'
        // #swagger.tags = ['Users']
		/* #swagger.parameters['user_mat'] = { 
            description: 'La matricola dell\'utente.',
            type: 'int'
        } */
		/* #swagger.parameters['email'] = { 
            description: 'L\'email nuova.',
            type: 'string'
        } */

		try{
			let matchingUser = User.findByMatricola(req.query.mat);
			if(matchingUser != null && UtilEmail.validateEmail(req.query.email) && req.query.email != null){
				/* istanbul ignore next */
				User.change(req.query.mat, req.query.email || matchingUser.email)
				res.status(201);
				res.json({message: 'User correctly modified!'})
			}else{
				res.status(400);
				res.json({message: 'ERROR 400: User not found!'});
			}
		}catch(error)/* istanbul ignore next */{
			res.status(500);
			res.json({message: 'ERROR 500: Local server error!'});
		}
	});

module.exports = usersRoutes;