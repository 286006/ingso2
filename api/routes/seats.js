const express = require('express');
const seatsRoutes = express.Router();

const Seat = require('../models/seats');
const Book = require('../models/book')

seatsRoutes.route('/')
    .get(async function(req, res) {

        // #swagger.description = 'Visualizza tutti i posti creati.'
        // #swagger.tags = ['Seats']

        try{
            let seats = await Seat.find({})
            if(seats != null){
                res.status(200);
                res.json([seats, {message: 'List of seats found!'}]);
            }else{
                res.status(404);
                res.json({message: 'ERROR 404: No seats found!'});
            }
        }catch(error)/* istanbul ignore next */{
			res.status(500);
			res.json({message: 'ERROR 500: Local server error!'});
		}
    })
    .delete(async function(req, res) {

        // #swagger.description = 'Elimina tutti i posti creati.'
        // #swagger.tags = ['Seats']

        try{
            /* istanbul ignore next */
            if(Seat.delete() && Book.delete()){
                res.status(200);
                res.json({message: 'List of seats deleted!'});
            }
        }catch(error)/* istanbul ignore next */{
			res.status(500);
			res.json({message: 'ERROR 500: Local server error!'});
		}
    })

seatsRoutes.route('/:nseats')
    .post(async function(req,res){

        // #swagger.description = 'Inserisce n posti a sedere'
        // #swagger.tags = ['Seats']
        /* #swagger.parameters['nseats'] = { 
            description: 'Il numero di posti a sedere da inserire.',
            type: 'int'
        } */

        try{
            var numberOfSeat = req.params.nseats;
            var saved=[];
            for(let i=0 ; i<numberOfSeat; i++) {
                var seat = new Seat();
                seat.booked = false;
                let s=await seat.save();
                saved.push(s);
            }
            if(saved.length > 0){
                res.status(201);
                res.json([saved , {message: 'Seats correctly created'}]);
            }else{
                res.status(400);
                res.json({message: 'ERROR 400: Seats not created!'});
            }
        }catch(error)/* istanbul ignore next */{
			res.status(500);
			res.json({message: 'ERROR 500: Local server error!'});
		}
    });

module.exports = seatsRoutes;
