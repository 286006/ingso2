const swaggerAutogen = require('swagger-autogen')()

const outputFile = './swagger_output.json'
const endpointsFiles = ['./app.js']

const doc = {
    info: {
        version: "1.0.0",
        title: "BookMeal",
        description: "Documentazione riguardanti le API di Seats&Meal, un servizio per prenotare il posto in mensa, visualizzare e valutare i pasti."
    },
    host: "https://cloudedgecomputing.herokuapp.com/",
    basePath: "/",
    schemes: ['http', 'https'],
    consumes: ['application/json'],
    produces: ['application/json'],
    tags: [
        {
            "name": "Home",
            "description": "Homepage."
        },
        {
            "name": "Book",
            "description": "Servizio di prenotazione del posto in mensa."
        },
        {
            "name": "Meal",
            "description": "Servizio di gestione dei pasti."
        },
        {
            "name": "Review",
            "description": "Servizio di recensioni dei pasti."
        },
        {
            "name": "Seats",
            "description": "Servizio di gestione dei posti."
        },
        {
            "name": "Users",
            "description": "Servizio di gestione degli utenti."
        },
    ]
}


swaggerAutogen(outputFile, endpointsFiles, doc).then(() => {
    require('./app.js')
})
